package pdm.ifsc.com.br.elzin_quiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class quiz extends AppCompatActivity {

    TextView pergunta;
    RadioButton rbResposta1, rbResposta2, rbResposta3, rbResposta4;
    int respostaCerta = R.id.rbResposta1;
    RadioGroup rgRespostas;
    int pontos = 0;

    List<Questao> questoes = new ArrayList<Questao>(){
        {
            add(new Questao("Qual o melhor aluno do I6(2016-2)?", R.id.rbResposta2, "Sarah Carolina", "LUCCAS ZIN ALVES", "Thiago César", "Elton Lemos"));
            add(new Questao("Como o Thanos morreu ?", R.id.rbResposta1, "Homem Formiga entrou no orificio anal dele, e ficou gigante", "Thor marretou sua cabeça", "Naruto chegou com seus clones", "Morreu por causa de sua depressão"));
            add(new Questao("Quem criou a terra?", R.id.rbResposta2, "Albert Einstein", "Alan Turing (pai da Ciência e da Computação,ateu e homossexual)", "Deus", "Sheldon Cooper"));
            add(new Questao("Quem é o melhor professor do IFSC Gaspar?", R.id.rbResposta4, "Prof. Luis Arcaro", "Thiago Paes", "Prof.Leonardo fernandes", "Prof. Rômulo S2<3"));
            add(new Questao("Qual é o vingador preferido do Elton?", R.id.rbResposta3, "Mickey", "Bob Esponja", "Batman", "Goku"));
            add(new Questao("Quem é o aluno mais burguês do IFSC Gaspra?", R.id.rbResposta4, "Vinicius", "Pablo", "Barbará", "Sarinha"));

        }
    };

    private void carregarQuestao(){
        if(questoes.size() > 0) {
            Questao q = questoes.remove(0);
            pergunta.setText(q.getPergunta());
            List<String> resposta = q.getRespostas();
            rbResposta1.setText(resposta.get(0));
            rbResposta2.setText(resposta.get(1));
            rbResposta3.setText(resposta.get(2));
            rbResposta4.setText(resposta.get(3));
            respostaCerta = q.getRespostaCerta();
            rgRespostas.setSelected(false);
        }
        else{ //acabaram as questões
            Intent intent = new Intent(this, resposta.class);
            intent.putExtra("pontos", pontos);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        getSupportActionBar().hide();

        pergunta = (TextView)findViewById(R.id.pergunta);
        rbResposta1 = (RadioButton)findViewById(R.id.rbResposta1);
        rbResposta2 = (RadioButton)findViewById(R.id.rbResposta2);
        rbResposta3 = (RadioButton)findViewById(R.id.rbResposta3);
        rbResposta4 = (RadioButton)findViewById(R.id.rbResposta4);
        rgRespostas = (RadioGroup)findViewById(R.id.rgRespostas);
        carregarQuestao();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        carregarQuestao();
    }

    public void btnResponderOnClick(View v){
        RadioButton rb = (RadioButton)findViewById(rgRespostas.getCheckedRadioButtonId());
        Intent intent = new Intent(this, resposta.class);
        if(rgRespostas.getCheckedRadioButtonId() == respostaCerta) {
            intent.putExtra("acertou", true);
            pontos++;
        }
        else intent.putExtra("acertou", false);
        intent.putExtra("pontos", pontos);
        startActivity(intent);
        rb.setChecked(false);
    }
}